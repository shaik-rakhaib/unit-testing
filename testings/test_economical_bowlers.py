import unittest
import sys
import coverage

cov = coverage.Coverage()
cov.start()

sys.path.append("./")
from solutions.economical_bowlers import execute

class Testeconomical(unittest.TestCase):
    def test_econmical(self):
        ans={'Sandeep Sharma': 5, 'M Morkel': 6}
        self.assertEquals(execute(),ans)
        
if __name__=='__main__':
    unittest.main() 

cov.stop()
cov.save()
