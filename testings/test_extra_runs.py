import unittest
import sys
import coverage

cov = coverage.Coverage()
cov.start()

sys.path.append("./")
from solutions.runs_conceded import execute

class Testeconomical(unittest.TestCase):
    def test_runs_conceded(self):
        ans={'Rising Pune Supergiants': 0, 'Royal Challengers Bangalore': 0}
        self.assertEquals(execute(),ans)
        
if __name__=='__main__':
    unittest.main()
    
cov.stop()
cov.save()
