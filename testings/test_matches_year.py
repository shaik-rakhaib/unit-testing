import unittest
import sys
import coverage

cov = coverage.Coverage()
cov.start()

sys.path.append("./")
from solutions.matches_year import execute

class Testmatchesyear(unittest.TestCase):
    def test_matches_year(self):
        ans={'2016': 3, '2015': 4, '2017': 5, '2008': 3}
        self.assertEquals(execute(),ans)
        
if __name__=='__main__':
    unittest.main() 
    
    
cov.stop()
cov.save()
