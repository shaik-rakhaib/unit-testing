import unittest
import sys
import coverage

cov = coverage.Coverage()
cov.start()

sys.path.append("./")
from solutions.year_team_matches import execute

class Testyearteammatches(unittest.TestCase):
    def test_year_team_matches(self):
        ans={'2016': {'Rising Pune Supergiants': 1,
                      'Kings XI Punjab': 1, 'Royal Challengers Bangalore': 1,
                      'Delhi Daredevils': 1, 'Mumbai Indians': 1, 'Sunrisers Hyderabad': 1},
             '2015': {'Rajasthan Royals': 1, 'Kings XI Punjab': 1, 'Sunrisers Hyderabad': 1, 'Kolkata Knight Riders': 1, 'Chennai Super Kings': 1, 'Royal Challengers Bangalore': 1, 'Delhi Daredevils': 1, 'Mumbai Indians': 1},
             '2017': {'Sunrisers Hyderabad': 1, 'Royal Challengers Bangalore': 2, 'Mumbai Indians': 1, 'Rising Pune Supergiant': 2, 'Gujarat Lions': 1, 'Kolkata Knight Riders': 1, 'Kings XI Punjab': 1, 'Delhi Daredevils': 1},
             '2008': {'Chennai Super Kings': 1, 'Delhi Daredevils': 1, 'Deccan Chargers': 1, 'Royal Challengers Bangalore': 1, 'Kings XI Punjab': 1, 'Kolkata Knight Riders': 1}}
        self.assertEquals(execute(),ans)
        
if __name__=='__main__':
    unittest.main() 
    

cov.stop()
cov.save()
