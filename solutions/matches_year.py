'''
 total matches played per year
'''
import csv

def calc_matches_by_year():
    '''calculate matches played per year throughout IPL'''
    with open('./csv_files/mock_matches.csv','r',encoding="utf-8") as csv_file:
        match=csv.DictReader(csv_file)
        year_by_matches={}
        for row in match:
            if row['season'] not in year_by_matches:
                year_by_matches[row['season']]=1
            else:
                year_by_matches[row['season']]+=1
        return year_by_matches  


def execute():
    '''the execution function'''
    matches_by_year=calc_matches_by_year()
    year=list(matches_by_year.keys())
    no_of_matches=list(matches_by_year.values())
    return matches_by_year

execute()
