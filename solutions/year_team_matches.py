'''
year vs no. of matches played by every team
'''
import csv

def calc_year_matches_team():
    '''transforms the csv file into the data structure required by the library'''
    with open('./csv_files/mock_matches.csv','r',encoding='utf-8') as csv_file:
        ipl_data=csv.DictReader(csv_file)
        matches_per_year={}
        for row in ipl_data:
            if row['season'] not in matches_per_year:
                matches_per_year[row['season']]={}
            if row['team1'] not in matches_per_year[row['season']]:
                matches_per_year[row['season']][row['team1']]=1
            else:
                matches_per_year[row['season']][row['team1']]+=1
            if row['team2'] not in matches_per_year[row['season']]:
                matches_per_year[row['season']][row['team2']]=1
            else:
                matches_per_year[row['season']][row['team2']]+=1
        return matches_per_year


def execute():
    '''execution starts'''
    matches_per_year=calc_year_matches_team()
    print(matches_per_year)
    return matches_per_year


execute()
