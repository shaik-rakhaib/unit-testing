'''
    plots the top 10 economical bowlers in the year 2015
'''
import csv

def calc_top_10_economical_bowlers():
    '''calculate the top economical bowlers in 2015'''
    matches_2015=[]
    with open('./csv_files/mock_matches.csv','r',encoding='utf-8') as csv_file:
        match_data=csv.DictReader(csv_file)
        for row in match_data:
            if row['season']=='2015':
                matches_2015.append(row['id'])
    with open('./csv_files/mock_deliveries.csv','r',encoding='utf-8') as csv_file:
        match=csv.DictReader(csv_file)
        top_bowlers={}
        for row in match:
            if row['match_id'] in matches_2015:
                if row['bowler'] not in top_bowlers:
                    top_bowlers[row['bowler']]=(int)(row['total_runs'])
                else:
                    top_bowlers[row['bowler']]+=(int)(row['total_runs'])
        for runs in top_bowlers.values():
            runs/=6
        top_n_bowlers=(sorted(top_bowlers.items(),key=lambda item: item[1]))
        top_bowlers=dict(top_n_bowlers[:10])
        return top_bowlers


def execute():
    '''the execution function'''
    top_10_economical_bowlers=calc_top_10_economical_bowlers()
    return top_10_economical_bowlers


execute()
