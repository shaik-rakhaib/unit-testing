'''
 extra runs conceded per teams in the year 2016
'''
import csv

def calc_extra_runs_in_2016():
    '''Calculates all the extra runs conceded per teams in the year 2016'''
    matchid_in_2016=[]
    with open('./csv_files/mock_matches.csv','r',encoding='utf-8') as csv_file:
        match_data=csv.DictReader(csv_file)
        for row in match_data:
            if row['season']=='2016':
                matchid_in_2016.append(row['id'])
    with open('./csv_files/mock_deliveries.csv','r',encoding='utf-8') as csv_file:
        runs_data=csv.DictReader(csv_file)
        extra_runs_by_team={}
        for row in runs_data:
            if row['match_id'] in matchid_in_2016:
                if row['batting_team'] not in extra_runs_by_team:
                    extra_runs_by_team[row['batting_team']]=int(row['extra_runs'])
                else:
                    extra_runs_by_team[row['batting_team']]+=int(row['extra_runs'])
        return extra_runs_by_team


def execute():
    '''the execution function'''
    extra_runs_per_team=calc_extra_runs_in_2016()
    print(extra_runs_per_team)
    return extra_runs_per_team


execute()
